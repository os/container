# Easy development container
This repository offers an easy-to-use environment setup for programming assignments:

<https://cs-mst.gitlab.io/index/ClassGeneral/ClassroomCode.html>

<https://cs-mst.gitlab.io/index/ClassGeneral/WorkingEnvironment.html>

## TLDR: To run the class container
On Windows in WSL, or x86 Mac in zsh shell, `git clone <your-repo>`, 
then run this in your assignment repository directory:
```
podman pull git-docker.mst.edu/os/container && podman run --interactive --tty --rm --mount type=bind,source="$(pwd)"/,target=/your_code --workdir=/your_code git-docker.mst.edu/os/container fish
```

On ARM Mac:
Build it yourself then run (see below).

On Linux:
You may not need the container at all, but read on if you want.

On Campus Mill desktop:
See below.

## Install Podman or Docker
Docker and Podman are two containerization suites that both support Open Container Initiative (OCI) containers.
Virtually all of their sub-commands are the same, so learning one will teach you the other.
For most commands, the documentation for Docker is interchangeable with that of Podman.
For all of the following command examples, depending on your machine,
you could replace `podman` with `sudo docker`, and the command should work identically.
However, I suggest `podman`, because it is better engineered, and has no disadvantages I have seen.
There are some subtle differences in security (podman has better security) and networking.

### Linux/Unix
If you are on Linux/Unix, then you should likely use `podman` in the host system repositories,
and not install based on the versions present on their websites.
For example, on Fedora do this:

```
sudo dnf install podman podman-compose
```

### Mac
If you are on Mac, then see the instructions here:

https://podman.io/docs/installation

### Windows
If you are on Windows, first install:

https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux

To do so, run Windows PowerShell as administrator, and within it type this:
```
wsl --install
```
Then, fully reboot Windows itself.
Then you can install containerization in one of three ways,
in order of your likely preference:

1. Within the WSL pseudo-Ubuntu install itself -- Install with: `sudo apt update && sudo apt install podman*`.
2. Within a full Linux VM -- Use the commands as above for Linux install. You will likely need the `:Z` option below.
3. In your Windows host itself -- Download from the urls above listed for Mac install. While podman will run this way, `git clone` on Windows host will break repositories by injecting Windows newlines into them... You can change Git's settings on Windows to not inject newlines.

## Test running a basic container works
After installing `podman`, open a terminal, and then run:
```
podman pull fedora
podman run --interactive --tty --rm fedora
```

Confirm that it worked, and you're in a Fedora container:

`cat /etc/os-release`

Then, to leave the container, type:

`exit`

## Running the remote pre-built image
If you are on an x86-64 architecture, then you can just pull a pre-built image.
Run the following command from the root directory of your Git repository.
It checks to see if a new version exists, and if needed, pulls it, and then runs.

```
podman pull git-docker.mst.edu/os/container && podman run --interactive --tty --rm --mount type=bind,source="$(pwd)"/,target=/your_code --workdir=/your_code git-docker.mst.edu/os/container fish
```

I suggest the `fish` shell, but you can run whichever shell you like,
by changing the last part of the command.

Your repository files are mounted inside the following directory:

`/your_code/`

## Getting a new version of the container
If you want to update your container to the latest verision, run this:
```
podman pull git-docker.mst.edu/os/container
```

## Building and running the image locally (optional)
If you are not on an x86-64 architecture,
or are curious, or are security-conscious,
you may want to build the image yourself.
From within the `container` repository directory 
(usually a Git sub-module of your assignment), execute:

```
podman build --tag container .
```

Then, you can run the image you built locally.
From the root directory of your assignment Git repository,
or any directory that you would like to mount in a container, execute:

```
podman run --interactive --tty --rm --mount type=bind,source="$(pwd)"/,target=/your_code --workdir=/your_code container fish
```

Your files will be mounted inside the container,
in the following directory:

`/your_code/`

## If you encounter permissions errors, or running gdb fails
These errors are likely due to SELinux or AppArmor:

<https://www.redhat.com/sysadmin/container-permission-denied-errors>

You may need to passthrough SElinux permissions in the `run` command, with Z/z:
```
# To make sure you have the latest container (or build it from the latest Containerfile):
podman pull git-docker.mst.edu/os/container
# Then run the container:

# Long format:
podman run --interactive --tty --rm --mount type=bind,source="$(pwd)"/,target=/your_code,Z --workdir=/your_code git-docker.mst.edu/os/container fish

# Short format:
# Z is Less permissive:
podman run -it -v $(pwd)/:/your_code/:Z --workdir=/your_code/ git-docker.mst.edu/os/container fish
# z is More permissive:
podman run -it -v $(pwd)/:/your_code/:z --workdir=/your_code/ git-docker.mst.edu/os/container fish
```
If that does not work, 
or if you run into permissions erros with the network,
then try disabling security broadly:
```
podman run --privileged -it -v $(pwd)/:/your_code/ --workdir=/your_code/ git-docker.mst.edu/os/container fish
```

Or in a compose file:
```
...
    volumes:
      - ./:/your_code:z
...
```

## Campus Linux Machines (rootless Singularity / Apptainer container)
If you would like te use campus resources to run a container, see:

<https://cs-mst.gitlab.io/index/ClassGeneral/WorkingEnvironment.html#campus-linux-machines-mill-desktop>

After logging into any of the campus Linux machine offerings,
in the virtual Linux terminal, first type `cd` to head to your home directory, then type:

On the campus machines, ese this shared copy, so we don't pollute the filesystem:
```
apptainer shell --shell /usr/bin/fish /mnt/stor/ceph/cs_class/containers/container.sif
```
```
# Or if your install of apptainer has the command `singularity` aliased instead:
singularity shell --shell /usr/bin/fish /mnt/stor/ceph/cs_class/containers/container.sif
```

This one unecssarily copies an extra sif to your home (don't use this one on campus).
```
apptainer shell --shell /usr/bin/fish docker://git-docker.mst.edu/os/container
```
```
# Or if your install of apptainer has the command `singularity` aliased instead:
singularity shell --shell /usr/bin/fish docker://git-docker.mst.edu/os/container
```

After that, your entire home directory should be accessible from within the container.

## Networking containers
To expose a port, for example 6789:
```
podman run --interactive --tty --rm -p 6789:6789 --mount type=bind,source="$(pwd)"/,target=/your_code --workdir=/your_code git-docker.mst.edu/os/container fish
```

Or, more verbosely using the long syntax format (which may not work on old versions of podman/docker), for example when exposing port 6789:
```
podman run --interactive --tty --publish target=6789,published=127.0.0.1:6789,protocol=tcp --mount type=bind,source="$(pwd)"/,target=/your_code --workdir=/your_code git-docker.mst.edu/os/container fish
```

This allows you to access network programs or services running within the container,
from your host.
For example, you could connect a web browser in the host,
to a web server in the container.

To get two containers connected together, you can create a network, and launch them both with it:
```
# If you don't see a podman network, then create it:
podman network ls 

# This part may already done by default on some platforms
podman create network -d bridge podman  

# Launch one or many containers on the same network:
podman run --interactive --tty --network=podman --mount type=bind,source="$(pwd)"/,target=/your_code --workdir=/your_code git-docker.mst.edu/os/container fish
```

Podman also offers pods, which allow containers to network together.

## Docker Compose
If you would like to launch your runtime configuration from a `compose.yaml` file, 
then run the following to enter a shell:
```
podman-compose run --rm programming
```

If you would like the jupyter notebooks containerized for CS5700, 
then edit the compose file to match your filesystem, and run:
```
podman-compose up --detach jupyter
```
Then, connect to the notebook at `http://localhost:8888`.

Notes:
`sudo docker-compose` is the Docker alternative to `podman-compose`.
On some systems, `sudo docker-compose` may be `sudo docker compose`.

# Want to learn all about containers?
Only install code locally ;)

https://www.youtube.com/watch?v=J0NuOlA2xDc

## Containers broadly:
https://en.wikipedia.org/wiki/OS-level_virtualization

https://en.wikipedia.org/wiki/Open_Container_Initiative

## Learn Podman
Podman is a better-engineered containerization suite compatible with OCI containers and Docker.
It's commands are mostly the same as Docker's.

https://docs.podman.io/en/latest/

## Learn Docker
https://en.wikipedia.org/wiki/Docker_(software)

The documentation of Docker supplements that of Podman,
since they mostly use the same basic commands.

Read the following:
https://docs.docker.com/get-started/overview/

Do this tutorial:
https://www.docker.com/101-tutorial/

Read the networking sub-sections of the documentation:
https://docs.docker.com/network/

Skim the API reference:
https://docs.docker.com/engine/reference/

For example, Dockerfiles, which specify how to build a container:
https://docs.docker.com/engine/reference/builder/

Or, docker run, which runs containers:
https://docs.docker.com/reference/cli/docker/container/run/
https://docs.docker.com/engine/reference/run/

Or, compose files, which are kind of like Makefiles for running multiple docker containers:
https://docs.docker.com/compose/ 
and 
https://docs.docker.com/compose/compose-file/

## Learn Singularity/Apptainer
You may prefer singularity / apptainer if you need to do HPC work,
or you are working on the campus virtual Linux machines:

https://en.wikipedia.org/wiki/Singularity_(software)

https://apptainer.org/user-docs/master/introduction.html

## Maintenance
If you maintain or update this container:

### Adding this submodule to your repo
If you, as an assignment developer or repository owner,
want to add this submodule to your repo, then:

```
git submodule add https://git.mst.edu/os/container/
```

To update thes sub-module, 
then within the parent repository, do this:

```
git submodule update --remote
```

### Adding software
The container image is hosted on git-docker.mst.edu.
You could host such an image at any container registry.
The software in the image can be changed or updated.
After you have created a token in the gitlab web interface,
which will be required to copy-paste as your password,
I push a new locally built container to the registry via:

```sh
# Edit the Containerfile, which the Dockerfile is linked to.
# If you want to build for apptainer directly, then run ./sif_generator.sh
# Note: It's easier to just have apptainer pull the pre-built image.

# Login will query your username and token
podman login git-docker.mst.edu

# Build it locally
podman build --tag git-docker.mst.edu/os/container .

# Test it works as you think, verify by running it and checking:
podman run --interactive --tty --rm git-docker.mst.edu/os/container fish
# Then explore around, and to leave, type: `exit`

# After verifying it's good, then push/upload
podman push git-docker.mst.edu/os/container
```

If you were to do the same for your own hosted image, 
then the url would differ.
To create your own container registry:
1. Create a repository on gitlab
2. Find the deploy menu, with container registry sub-menu.
3. Use the information there and the above commands to upload your own version.
