#!/bin/bash
# If you want to generate apptainer sif files directly,
# and then use them to build.
# It's easier to just do this instead:
# apptainer shell <path>

pip3 install --upgrade spython
spython recipe Dockerfile >apptainer.sif
